const defaultState = {
    cart: [],
    isLoading: false,
    error: {}
};

export default (state = defaultState, action = {}) => {
    switch (action.type) {
        case 'INCREMENT_CART': {
            return {
                cart: state.cart.map((food) => {
                    if (action.payload == food.id) {
                        food.count += 1;
                    }
                    return food;
                }),
                ...state,
            };
        }

        case 'SET_COUNT_CART': {
            return {
                cart: state.cart.map((food) => {
                    if (action.payload == food.id) {
                        food.count = action.number;
                        if (food.count < 0) food.count = 0;
                    }
                    return food;
                }),       
                ...state
            };
        }

        case 'FETCH_CART_REQUEST': {
            return {
                ...state,
                isLoading: true
            };
        }

        case 'FETCH_CART_SUCCESS': {
            return {
                ...state,
                cart: action.payload.map((food) => {
                    food.count = 0;
                    return food;
                }),
                isLoading: false
            };
        }

        case 'FETCH_CART_FAILED': {
            return {
                ...state,
                cart: [],
                error: action.error,
                isLoading: false
            };
        }

        default:
            return state;
    }
};



import { combineReducers } from 'redux';

import authStoreReducer from './authReducer';
import customerCartReducer from './customerCartReducer';

const reducers = {
  authStore: authStoreReducer,
  cartStore: customerCartReducer
};

const rootReducer = combineReducers(reducers);

export default rootReducer;


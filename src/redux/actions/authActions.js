import { login } from '../../api';

export default function fetchLogin(username) {
    return async (dispatch) => {
        dispatch({
            type: 'FETCH_LOGIN_REQUEST'
        });

        try {
            const result = await login(username);

            if (result.status === 200) {
                dispatch({
                    type: 'FETCH_LOGIN_SUCCESS',
                    payload: result.data
                });
            } else {
                dispatch({
                    type: 'FETCH_LOGIN_FAILED',
                    error: result.data
                });
            }
        } catch (err) {
            dispatch({
                type: 'FETCH_LOGIN_FAILED',
                error: err
            });
        }
    };
}


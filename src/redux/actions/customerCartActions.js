import { fetchFoodData } from '../../api';

export function incrementToCart(item) {
    return async (dispatch) => {
        dispatch({
            type: 'INCREMENT_CART',
            payload: item
        });
    };
}

export function setCountToCart(item, number) {
    return async (dispatch) => {
        dispatch({
            type: 'SET_COUNT_CART',
            payload: item,
            number
        });
    };
}

export function fetchDataToCart(username, token) {
    return async (dispatch) => {
        dispatch({
            type: 'FETCH_CART_REQUEST'
        });

        try {
            const result = await fetchFoodData(username, token);

            if (result.status === 200) {
                dispatch({
                    type: 'FETCH_CART_SUCCESS',
                    payload: result.data.data.foods
                });
            } else {
                dispatch({
                    type: 'FETCH_CART_FAILED',
                    error: result.data
                });
            }
        } catch (err) {
            dispatch({
                type: 'FETCH_CART_FAILED',
                error: err
            });
        }
    }
}


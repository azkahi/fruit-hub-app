import  axios from 'axios';

async function fetchAxios(method, path, body, token) {
  try {
    const req = await axios({
      method,
      url: `https://api.irwinpratajaya.com/${path}`,
      data: body,
      headers: {
        "Authorization": token
      }
    });

    if (req.status !== 400) {
      return req;
    }
  } catch (e) {
    throw e;
  }

}

export default fetchAxios;

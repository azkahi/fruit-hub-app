import React from 'react';

import { Provider } from 'react-redux';

import store from './redux';

import MainNavigation from './router';

const App = () => {
  return (
    <Provider store={store}>
      <MainNavigation />
    </Provider>
  );
};

export default App;

import React from 'react';
import {Text, StyleSheet, TouchableOpacity} from 'react-native';

const Button = ({type, title, onPress}) => {
  return (
    <TouchableOpacity style={styles.container(type)} onPress={onPress}>
      <Text style={styles.text(type)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  container: (type) => ({
    backgroundColor: type === 'secondary' ? '#E5E5E5' : '#FFA451',
    borderRadius: 10,
    height: 56,
    marginHorizontal: 25,
    borderWidth: 1,
    borderColor: '#FFA451',
  }),
  text: (type) => ({
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 16,
    lineHeight: 24,
    textAlign: 'center',
    color: type === 'secondary' ? '#FFA451' : '#FFFFFF',
    paddingVertical: 15,
  }),
});

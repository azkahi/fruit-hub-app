import React from 'react';
import {StyleSheet, View, TextInput} from 'react-native';

const InputHolder = (props) => {
  return <TextInput style={styles.input} {...props} />;
};

export default InputHolder;

const styles = StyleSheet.create({
  input: {
    borderRadius: 10,
    height: 56,
    marginHorizontal: 25,
    backgroundColor: '#F3F1F1',
    padding: 14,
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 20,
    lineHeight: 28.6,
    color: '#9FA5C0',
  },
});

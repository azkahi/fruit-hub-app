import fetchAxios from '../utils';

export async function login(username) {
  try {
    const result = await fetchAxios('POST', 'login', {
      username
    });
    return result;
  } catch (error) {
    throw error;
  }
}

export async function fetchFoodData(username, token) {
  try {
    const result = await fetchAxios('GET', `home?username=${username}`, {}, token);

    return result;
  } catch (error) {
    throw error;
  }
}
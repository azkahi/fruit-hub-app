import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import {
  SplashScreen,
  WelcomeScreen,
  Authentication,
  OrderComplete,
  TrackOrder,
  Home,
  MyBasket,
  AddToBasket,
} from '../pages';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false
        }}
      >
        <Stack.Screen name="Splash" component={SplashScreen} />
        <Stack.Screen name="Welcome" component={WelcomeScreen} />
        <Stack.Screen name="Authentication" component={Authentication} />
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="AddToBasket" component={AddToBasket} />
        <Stack.Screen name="MyBasket" component={MyBasket} />
        <Stack.Screen name="OrderComplete" component={OrderComplete} />
        <Stack.Screen name="TrackOrder" component={TrackOrder} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

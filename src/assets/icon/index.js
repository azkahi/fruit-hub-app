import GreenCheck from './green-checklist.png';
import Arrow from './arrow.png';
import OrderTaken from './order-taken.png';
import OrderPrepared from './order-prepared.png';
import OrderDelivered from './order-delivered.png';
import PhoneCall from './phone-call.png';
import Love from './Vector.png';
import Minus from './minus.png';
import Plus from './plus.png';

export {
  GreenCheck,
  Arrow,
  OrderTaken,
  OrderPrepared,
  OrderDelivered,
  PhoneCall,
  Love,
  Minus,
  Plus,
};

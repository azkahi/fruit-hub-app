import FruitBasketWelcome from './fruit-basket1.png';
import FruitBasketAuth from './fruit-basket2.png';
import FruitDrops from './fruit-drops.png';
import EllipseWelcome from './ellipse-welcome.png';
import EllipseSmall from './ellipse-small.png';
import MapTrack from './map.png';
import Quinoa from './quinoa.png';

export {
  FruitBasketWelcome,
  FruitBasketAuth,
  FruitDrops,
  EllipseWelcome,
  EllipseSmall,
  MapTrack,
  Quinoa,
};

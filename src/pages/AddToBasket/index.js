import React, {useState, useEffect} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {Quinoa, Arrow, Love, Minus, Plus} from '../../assets';
import {Button} from '../../components';
import { connect } from 'react-redux';

import { setCountToCart } from '../../redux/actions/customerCartActions';

const AddToBasket = (props) => {
  const [food, setFood] = useState({});
  const [num, setNum] = useState(0);

  const { item } = props.route.params;
  useEffect(() => {
    setFood(item);
    setNum(item.count);
  }, []);

  return (
    <View style={styles.mainPage}>
      <View style={styles.topPage}>
        <TouchableOpacity style={styles.backButton} onPress={() => props.navigation.pop()}>
          <Image source={Arrow} />
          <Text style={styles.goBack}>Go back</Text>
        </TouchableOpacity>
        <Image source={{ uri: food.img }} style={styles.Quinoa} />
      </View>
      <View style={styles.bottomPage}>
        <Text style={styles.mainText}>{food.name}</Text>

        <View style={styles.priceRow}>
          <TouchableOpacity style={styles.minusButton} onPress={() => {
            food.count -= 1;
            if (food.count < 0) food.count = 0;
            setNum(food.count);
          }}>
            <Image source={Minus} style={{alignSelf: 'center'}} />
          </TouchableOpacity>
          <Text style={styles.quantity}>{num}</Text>
          <TouchableOpacity style={styles.plusButton} onPress={() => {
            food.count += 1;
            setNum(food.count);
          }}>
            <Image source={Plus} style={{alignSelf: 'center'}} />
          </TouchableOpacity>
          <Text style={styles.price}>{num * food.price}</Text>
        </View>

        <View
          style={{height: 1, backgroundColor: '#F3F3F3', marginVertical: 20}}
        />

        <Text style={styles.onePack}>One Pack Contains:</Text>
        <View
          style={{
            height: 1,
            backgroundColor: '#FFA451',
            marginBottom: 20,
            width: 150,
            marginLeft: 20,
          }}
        />
        <Text style={styles.contains}>
          {food.ingredients}
        </Text>

        <View
          style={{height: 1, backgroundColor: '#F3F3F3', marginVertical: 20}}
        />

        <Text style={styles.description}>
          {food.description}
        </Text>
        <View style={styles.bottomRow}>
          <TouchableOpacity style={styles.loveWrap}>
            <Image source={Love} style={styles.Love} />
          </TouchableOpacity>
          <Button title={'          Add to basket          '} onPress={() => {
            props.dispatchSetNumberToCart(food.id, food.count);
            props.navigation.navigate('MyBasket');
          }}/>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainPage: {
    height: '100%',
  },
  topPage: {
    backgroundColor: '#FFA451',
    flexDirection: 'column',
    flex: 0.4,
  },
  backButton: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    borderRadius: 100,
    alignItems: 'center',
    width: 80,
    height: 32,
    justifyContent: 'center',
    marginLeft: 20,
    marginTop: 20,
    marginBottom: 10,
  },
  goBack: {
    marginLeft: 5,
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 21,
  },
  Quinoa: {
    height: 176,
    width: 176,
    marginBottom: 10,
    alignSelf: 'center',
  },
  bottomPage: {
    backgroundColor: '#E5E5E5',
    flex: 0.6,
    flexDirection: 'column',
  },
  mainText: {
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 32,
    lineHeight: 32,
    marginTop: 30,
    marginLeft: 20,
  },
  priceRow: {flexDirection: 'row', marginTop: 20},
  minusButton: {
    marginHorizontal: 20,
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 100,
    width: 32,
    height: 32,
    justifyContent: 'center',
  },
  quantity: {
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 24,
    lineHeight: 34.32,
    alignItems: 'center',
    alignSelf: 'center',
  },
  plusButton: {
    marginHorizontal: 20,
    backgroundColor: '#FFF2E7',
    borderRadius: 100,
    width: 32,
    height: 32,
    justifyContent: 'center',
  },
  price: {
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 24,
    lineHeight: 34.32,
    alignItems: 'center',
    alignSelf: 'center',
    marginLeft: 95,
  },
  onePack: {
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 20,
    lineHeight: 32,
    marginLeft: 20,
  },
  contains: {
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 16,
    lineHeight: 24,
    marginLeft: 20,
  },
  description: {
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 14,
    lineHeight: 21,
    marginLeft: 20,
    width: 284,
  },
  bottomRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  loveWrap: {
    backgroundColor: '#FFF7F0',
    height: 48,
    width: 48,
    borderRadius: 100,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    marginLeft: 20,
  },
  Love: {
    alignSelf: 'center',
    alignItems: 'center',
  },
});

function mapStateToProps(state) {
  return {
    authStore: state.authStore,
    cart: state.cartStore
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchSetNumberToCart: (id, number) => dispatch(setCountToCart(id, number)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddToBasket);

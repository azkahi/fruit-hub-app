import { useLinkProps } from '@react-navigation/native';
import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {GreenCheck} from '../../assets';
import {Button} from '../../components';

const OrderComplete = (props) => {
  return (
    <View style={styles.mainPage}>
      <View style={styles.outline}>
        <Image source={GreenCheck} style={styles.check} />
      </View>

      <View>
        <Text style={styles.mainText}>Congratulations!!!</Text>
        <Text style={styles.secText}>
          Your order have been taken and is being attended to
        </Text>
      </View>

      <View style={styles.button}>
        <Button title={'Track Order'} onPress={() => props.navigation.navigate('TrackOrder')} />
      </View>

      <View style={styles.buttonSec}>
        <Button type={'secondary'} title={'Continue shopping'} onPress={() => props.navigation.navigate('Home')}/>
      </View>
    </View>
  );
};

export default OrderComplete;

const styles = StyleSheet.create({
  mainPage: {
    height: '100%',
    alignContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  outline: {
    backgroundColor: '#E0FFE5',
    borderRadius: 100,
    height: 164,
    width: 164,
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#4CD964',
    marginTop: 130,
  },
  check: {
    width: 100,
    height: 100,
    alignSelf: 'center',
  },
  mainText: {
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 32,
    lineHeight: 32,
    textAlign: 'center',
    marginTop: 40,
  },
  secText: {
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 20,
    lineHeight: 30,
    textAlign: 'center',
    marginTop: 20,
    width: 250,
  },
  button: {
    width: 200,
    marginTop: 40,
  },
  buttonSec: {
    width: 250,
    marginTop: 40,
  },
});

import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, FlatList, Modal } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

const MyBasket = (props) => {
  const [cart, setCart] = useState([]);
  const [total, setTotal] = useState(0);
  const [modalDeliveryVisible, setModalDeliveryVisible] = useState(false);
  const [modalPayWithCardVisible, setModalPayWithCardVisible] = useState(false);


  useEffect(() => {
    setCart(props.cart.cart.filter((element) => element.count > 0))

    const tempCart = props.cart.cart.filter((element) => element.count > 0);

    let tempTotal = 0;

    tempCart.forEach(food => {
      tempTotal = tempTotal + (food.price * food.count);
    });

    setTotal(tempTotal);
  }, [])

  const CartItem = ({ item }) => {
    return (
      <View style={styles.cartItemContainer}>
        <View style={styles.cartItemThumbnail}>
          <Image style={styles.cartItemImage}
            source={{
              uri: item.img,
            }} />
        </View>
        <View style={styles.cartItemTexts}>
          <Text style={styles.cartItemName}>{item.name}</Text>
          <Text style={styles.cartItemNum}>{item.count}packs</Text>
        </View>
        <View style={styles.cartItemPriceContainer}>
          <Text style={styles.cartItemPrice}>{item.count * item.price}</Text>
        </View>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        <TouchableOpacity style={styles.backButton} onPress={() => props.navigation.pop()}>
          <Icon name="chevron-back-outline" size={30} color="black" />
          <Text style={styles.backButtonText}>Go back</Text>
        </TouchableOpacity>
        <Text style={styles.textHeader}>My Basket</Text>
      </View>
      <View style={styles.cartContainer}>
        <FlatList
          data={cart}
          renderItem={CartItem}
          keyExtractor={(element) => element.id}
        />
      </View>
      <View style={styles.checkoutContainer}>
        <View style={styles.totalContainer}>
          <Text style={styles.totalText}>Total</Text>
          <Text style={styles.totalNum}>{total}</Text>
        </View>
        <TouchableOpacity style={styles.checkoutButton} onPress={() => setModalDeliveryVisible(true)}>
          <Text style={styles.checkoutButtonText}>Checkout</Text>
        </TouchableOpacity>
      </View>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalDeliveryVisible}
      >
        <View style={styles.modalContainer}>
          <TouchableOpacity style={styles.closeButton} onPress={() => setModalDeliveryVisible(false)}>
            <Icon name="close-outline" size={30} color="black" />
          </TouchableOpacity>
          <View style={styles.deliveryContainer}>
            <Text style={styles.modalTitleText}>Delivery address</Text>
            <TextInput style={styles.inputTextContainer} placeholder="10th avenue, Lekki, Lagos State" placeholderTextColor="#C2BDBD" />
            <Text style={styles.modalTitleText}>Number we can call</Text>
            <TextInput style={styles.inputTextContainer} placeholder="09090605708" placeholderTextColor="#C2BDBD" />
            <View style={styles.modalButtonsContainer}>
              <TouchableOpacity style={styles.modalButton} onPress={() => {
                setModalDeliveryVisible(false);
                props.navigation.navigate('OrderComplete');
              }}>
                <Text style={styles.modalButtonText}>Pay on delivery</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.modalButton} onPress={() => {
                setModalDeliveryVisible(false);
                setModalPayWithCardVisible(true);
              }}>
                <Text style={styles.modalButtonText}>Pay with card</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      <Modal
        animationType="slide"
        transparent={true}
        visible={modalPayWithCardVisible}
      >
        <View style={styles.modalContainer}>
          <TouchableOpacity style={styles.closeButton} onPress={() => setModalPayWithCardVisible(false)}>
            <Icon name="close-outline" size={30} color="black" />
          </TouchableOpacity>
          <View style={styles.deliveryContainer}>
            <Text style={styles.modalTitleText}>Card Holders Name</Text>
            <TextInput style={styles.inputTextContainer} placeholder="Adolphus Chris" placeholderTextColor="#C2BDBD" />
            <Text style={styles.modalTitleText}>Card Number</Text>
            <TextInput style={styles.inputTextContainer} placeholder="1234 5678 9012 1314" placeholderTextColor="#C2BDBD" />
            <View style={styles.modalDateCVV}>
              <View style={styles.modalFragment}>
                <Text style={styles.modalTitleText}>Date</Text>
                <TextInput style={styles.inputTextContainer} placeholder="10/30" placeholderTextColor="#C2BDBD" />
              </View>
              <View style={styles.modalFragment}>
                <Text style={styles.modalTitleText}>CVV</Text>
                <TextInput style={styles.inputTextContainer} placeholder="123" placeholderTextColor="#C2BDBD" />
              </View>
            </View>
            <View style={styles.modalCompleteOrderContainer}>
              <TouchableOpacity style={styles.modalCompleteOrderButton} onPress={() => {
                setModalPayWithCardVisible(false);
                props.navigation.navigate('OrderComplete');
              }}>
                <Text style={styles.modalButtonText}>Complete Order</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  headerContainer: {
    height: 100,
    backgroundColor: '#FFA451',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  backButton: {
    borderRadius: 60,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingRight: 10,
    marginLeft: 24
  },
  backButtonText: {
    fontSize: 16
  },
  textHeader: {
    color: 'white',
    fontSize: 24,
    marginLeft: 34
  },
  cartContainer: {
    flex: 1,
  },
  cartItemContainer: {
    flexDirection: 'row',
    marginHorizontal: 24,
    marginVertical: 24,
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  cartItemThumbnail: {
    padding: 10,
    borderRadius: 10,
    borderWidth: StyleSheet.hairlineWidth,
    justifyContent: 'center',
    alignItems: 'center'
  },
  cartItemImage: {
    width: 40,
    height: 40,
  },
  cartItemTexts: {
    marginLeft: 16,
    flexDirection: 'column',
  },
  cartItemName: {
    fontSize: 16
  },
  cartItemNum: {
    fontSize: 14
  },
  cartItemPriceContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginRight: 24
  },
  cartItemPrice: {
    fontSize: 16,
  },
  checkoutContainer: {
    flexDirection: 'row',
    marginHorizontal: 24,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  totalContainer: {
    flexDirection: 'column',
  },
  totalText: {
    fontSize: 16,
  },
  totalNum: {
    fontSize: 24
  },
  checkoutButton: {
    paddingHorizontal: 50,
    paddingVertical: 14,
    backgroundColor: '#FFA451',
    borderRadius: 10
  },
  checkoutButtonText: {
    fontSize: 16,
    color: 'white'
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  closeButton: {
    borderRadius: 60,
    backgroundColor: 'white',
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'center',
    width: 50,
    height: 50,
    alignSelf: 'center'
  },
  deliveryContainer: {
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    backgroundColor: 'white'
  },
  inputTextContainer: {
    borderRadius: 20,
    backgroundColor: '#F3F4F9',
    padding: 10,
    marginTop: 20,
    marginHorizontal: 24
  },
  modalTitleText: {
    fontSize: 20,
    fontWeight: 'bold',
    marginHorizontal: 24,
    marginVertical: 16
  },
  modalButtonsContainer: {
    flexDirection: 'row',
    marginHorizontal: 24,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 40
  },
  modalButton: {
    borderColor: '#FFA451',
    borderRadius: 10,
    borderWidth: 1,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalButtonText: {
    color: '#FFA451',
    fontSize: 16
  },
  modalDateCVV: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  modalFragment: {
    flex: 1
  },
  modalCompleteOrderContainer: {
    borderRadius: 20,
    backgroundColor: '#FFA451',
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 24
  },
  modalCompleteOrderButton: {
    backgroundColor: 'white',
    borderRadius: 10,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

function mapStateToProps(state) {
  return {
    authStore: state.authStore,
    cart: state.cartStore
  }
}

function mapDispatchToProps(dispatch) {
  return {

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MyBasket);

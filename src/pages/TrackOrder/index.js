import React from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import {
  OrderTaken,
  OrderPrepared,
  OrderDelivered,
  PhoneCall,
  GreenCheck,
  MapTrack,
  Arrow,
} from '../../assets';

const TrackOrder = (props) => {
  return (
    <View style={styles.mainPage}>
      <View style={styles.topPage}>
        <TouchableOpacity style={styles.backButton} onPress={() => props.navigation.pop()}>
          <Image source={Arrow} />
          <Text style={styles.goBack}>Go back</Text>
        </TouchableOpacity>
        <Text style={styles.textDelivery}>Delivery Status</Text>
      </View>
      <View style={styles.bottomPage}>
        {/* ROW1 */}
        <View style={styles.row}>
          <View style={styles.outerOrder}>
            <Image source={OrderTaken} style={styles.imageOrder} />
          </View>
          <Text style={styles.rowText}>Order Taken</Text>
          <Image source={GreenCheck} style={styles.imageCheck1} />
        </View>

        {/* ROW2 */}
        <View style={styles.row}>
          <View style={styles.outerPrepared}>
            <Image source={OrderPrepared} style={styles.imageOrder} />
          </View>
          <Text style={styles.rowText}>Order Is Being Prepared</Text>
          <Image source={GreenCheck} style={styles.imageCheck2} />
        </View>

        {/* ROW3 */}
        <View style={styles.row}>
          <View style={styles.outerPrepared}>
            <Image source={OrderDelivered} style={styles.imageOrder} />
          </View>
          <View style={styles.textDelivered}>
            <Text style={styles.rowTextDelivered1}>
              Order Is Being Delivered
            </Text>
            <Text style={styles.rowTextDelivered2}>
              Your delivery agent is coming
            </Text>
          </View>
          <Image source={PhoneCall} style={styles.phoneCall} />
        </View>

        {/* MAP */}
        <View style={{marginTop: 20}}>
          <Image source={MapTrack} style={styles.mapTrack} />
        </View>

        {/* LAST ROW */}
        <View style={styles.lastRow}>
          <View style={styles.outerReceived}>
            <Image source={GreenCheck} style={styles.imageReceived} />
          </View>
          <Text style={styles.rowText}>Order Received </Text>
          <View style={styles.tripleCheck}>
            <Image source={GreenCheck} style={styles.imageCheck3} />
            <Image source={GreenCheck} style={styles.imageCheck3} />
            <Image source={GreenCheck} style={styles.imageCheck3} />
          </View>
        </View>
      </View>
    </View>
  );
};

export default TrackOrder;

const styles = StyleSheet.create({
  mainPage: {
    height: '100%',
  },
  topPage: {
    backgroundColor: '#FFA451',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 0.15,
  },
  backButton: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    borderRadius: 100,
    alignItems: 'center',
    width: 80,
    height: 32,
    justifyContent: 'center',
    marginLeft: 20,
  },
  goBack: {
    marginLeft: 5,
    fontWeight: '400',
    fontSize: 16,
    lineHeight: 21,
  },
  textDelivery: {
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 24,
    lineHeight: 32,
    color: '#FFFFFF',
    marginLeft: 20,
  },
  bottomPage: {
    backgroundColor: '#E5E5E5',
    flex: 0.85,
    flexDirection: 'column',
    alignItems: 'center',
  },
  row: {
    flexDirection: 'row',
    marginTop: 40,
  },
  outerOrder: {
    backgroundColor: '#FFFAEB',
    borderRadius: 10,
    width: 65,
    height: 64,
    justifyContent: 'center',
  },
  imageOrder: {
    width: 48,
    height: 43.25,
    alignSelf: 'center',
  },
  rowText: {
    alignSelf: 'center',
    marginLeft: 20,
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 16,
    lineHeight: 22.88,
  },
  imageCheck1: {
    width: 24,
    height: 24,
    alignSelf: 'center',
    marginLeft: 122,
  },
  outerPrepared: {
    backgroundColor: '#F1EFF6',
    borderRadius: 10,
    width: 65,
    height: 64,
    justifyContent: 'center',
  },
  imageCheck2: {
    width: 24,
    height: 24,
    alignSelf: 'center',
    marginLeft: 50,
  },
  outerDelivered: {
    backgroundColor: '#FEF0F0',
    borderRadius: 10,
    width: 65,
    height: 64,
    justifyContent: 'center',
    marginLeft: 24,
  },
  textDelivered: {
    flexDirection: 'column',
    justifyContent: 'center',
    marginLeft: 20,
  },
  rowTextDelivered1: {
    textAlign: 'left',
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 16,
    lineHeight: 22.88,
  },
  rowTextDelivered2: {
    textAlign: 'left',
  },
  phoneCall: {
    width: 40,
    height: 40,
    alignSelf: 'center',
    marginLeft: 23,
  },
  mapTrack: {
    width: 310,
    height: 128,
    borderRadius: 10,
  },
  lastRow: {
    flexDirection: 'row',
    marginTop: 20,
  },
  outerReceived: {
    backgroundColor: '#F0FEF8',
    borderRadius: 10,
    width: 65,
    height: 64,
    justifyContent: 'center',
  },
  imageReceived: {
    width: 40,
    height: 40,
    alignSelf: 'center',
  },
  tripleCheck: {
    flexDirection: 'row',
    marginLeft: 60,
  },
  imageCheck3: {
    width: 12,
    height: 12,
    alignSelf: 'center',
    marginLeft: 10,
  },
});

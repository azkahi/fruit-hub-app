import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, FlatList, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import { fetchDataToCart, incrementToCart } from '../../redux/actions/customerCartActions';

const Home = (props) => {
  const [cart, setCart] = useState([]);
  const [activeCategory, setActiveCategory] = useState('hottest');

  const FoodBox = ({ item }) => {
    return (
      <TouchableOpacity style={styles.containerFoodBox} onPress={() => {
        props.navigation.navigate('AddToBasket', {
          item
        })
      }}>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
          <Icon name="heart-outline" size={20} color="orange" />
        </View>
        <View style={styles.foodBoxImageContainer}>
          <Image
            style={styles.foodBoxImage}
            source={{
              uri: item.img,
            }}
          />
        </View>
        <Text style={{textAlign: 'center'}}>{item.name}</Text>
        <View style={styles.priceButton}>
            <Text style={{color: 'orange'}}>{item.price}</Text>
            <TouchableOpacity onPress={() => props.dispatchIncrement(item.id)}>
              <Icon name="add-circle-outline" size={20} color="orange" />
            </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  };

  useEffect(() => {
    props.dispatchFetchFood(props.authStore.payload.data.username, props.authStore.payload.data.token);
  }, []);

  useEffect(() => {
    setCart(props.cart.cart);
  }, [props.cart]);

  return (
    <ScrollView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity style={styles.basketIcon} onPress={() => props.navigation.navigate('MyBasket')}>
          <Icon name="basket-outline" size={30} color="orange" />
          <Text style={styles.basketText}>My Basket</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.headerWelcome}>
        <Text style={styles.headerWelcomeTitle}>
          Hello {props.authStore.payload.data.username}, <Text style={{ fontWeight: 'bold' }}>What fruit salad combo do you want today?</Text>
        </Text>
      </View>
      <TextInput style={styles.searchContainer} placeholder="Search for fruit salad combos" placeholderTextColor="#86869E">
        <Icon style={{ marginRight: 20 }} name="search-outline" size={30} color="#86869E" />
      </TextInput>
      <Text style={styles.recommendedComboText}>Recommended Combo</Text>
      <View style={styles.recommendedComboContainer}>
        <FlatList
          data={cart.filter(food => food.recommended)}
          renderItem={FoodBox}
          keyExtractor={food => food.id}
          horizontal
        />
      </View>
      <View style={styles.choiceContainer}>
        <TouchableOpacity onPress={() => setActiveCategory('hottest')}>
          <Text style={activeCategory == 'hottest' ? styles.activeCategoryButton : styles.categoryButton}>Hottest</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setActiveCategory('popular')}>
          <Text style={activeCategory == 'popular' ? styles.activeCategoryButton : styles.categoryButton}>Popular</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setActiveCategory('newCombo')}>
          <Text style={activeCategory == 'newCombo' ? styles.activeCategoryButton : styles.categoryButton}>New Combo</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => setActiveCategory('top')}>
          <Text style={activeCategory == 'top' ? styles.activeCategoryButton : styles.categoryButton}>Top</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.categoryContainer}>
        <FlatList
          data={cart.filter(food => food[activeCategory])}
          renderItem={FoodBox}
          keyExtractor={food => food.id}
          horizontal
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  basketIcon: {
    marginTop: 32,
    marginRight: 32,
    justifyContent: 'center',
    alignItems: 'center'
  },
  basketText: {
    fontSize: 10,
    color: 'black'
  },
  headerWelcome: {
    marginLeft: 24,
    marginTop: 20,
    marginRight: 94
  },
  headerWelcomeTitle: {
    fontSize: 20
  },
  searchContainer: {
    borderRadius: 20,
    backgroundColor: '#F3F4F9',
    padding: 10,
    marginTop: 20,
    marginHorizontal: 24
  },
  recommendedComboText: {
    marginLeft: 24,
    marginTop: 20,
    fontSize: 24,
    fontWeight: 'bold'
  },
  containerFoodBox: {
    borderRadius: 20,
    padding: 20,
    marginHorizontal: 20,
    width: 180,
    borderWidth: StyleSheet.hairlineWidth,
  },
  foodBoxImageContainer: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  foodBoxImage: {
    height: 80,
    width: 80,
    marginVertical: 10
  },
  priceButton: {
    flexDirection: 'row',
    marginTop: 5,
    justifyContent: 'space-between'
  },
  recommendedComboContainer: {
    marginTop: 10
  },
  choiceContainer: {
    flexDirection: 'row',
    marginLeft: 24,
    marginTop: 40,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  activeCategoryButton: {
    fontWeight: 'bold',
    fontSize: 24,
    borderBottomWidth: 1,
    borderBottomColor: 'orange'
  },
  categoryButton: {
    fontSize: 16
  },
  categoryContainer: {
    marginTop: 20
  }
});

function mapStateToProps(state) {
  return {
    authStore: state.authStore,
    cart: state.cartStore
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchFetchFood: (username, token) => dispatch(fetchDataToCart(username, token)),
    dispatchIncrement: (id) => dispatch(incrementToCart(id))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);

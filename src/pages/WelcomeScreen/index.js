import React from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {FruitBasketWelcome, FruitDrops, EllipseWelcome} from '../../assets';
import {Button} from '../../components';

const WelcomeScreen = (props) => {
  return (
    <View style={styles.mainPage}>
      <View style={styles.topPage}>
        <Image source={FruitDrops} style={styles.fruitDrops} />
        <Image source={FruitBasketWelcome} style={styles.fruitBasket} />
        <Image source={EllipseWelcome} />
      </View>
      <View style={styles.bottomPage}>
        <Text style={styles.mainText}>Get The Freshest Fruit Salad Combo</Text>
        <Text style={styles.secondText}>
          We deliver the best and freshest fruit salad in town. Order for a
          combo today!!!
        </Text>
        <Button title={'Let’s Continue'} onPress={() => props.navigation.navigate('Authentication')} />
      </View>
    </View>
  );
};

export default WelcomeScreen;

const styles = StyleSheet.create({
  mainPage: {
    height: '100%',
  },
  topPage: {
    backgroundColor: '#FFA451',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    flex: 0.65,
  },
  fruitDrops: {
    width: 50,
    height: 37.52,
    marginLeft: 260,
    marginTop: 40,
  },
  fruitBasket: {
    height: 260,
    width: 301,
    marginBottom: 10,
  },
  bottomPage: {
    backgroundColor: '#E5E5E5',
    flex: 0.4,
    flexDirection: 'column',
  },
  mainText: {
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 20,
    lineHeight: 28.6,
    marginTop: 30,
    marginLeft: 25,
  },
  secondText: {
    fontWeight: '400',
    fontStyle: 'normal',
    fontSize: 16,
    lineHeight: 24,
    marginTop: 8,
    marginLeft: 25,
    width: 300,
    marginBottom: 40,
  },
});

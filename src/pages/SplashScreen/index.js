import React, { useEffect } from 'react';
import {Text} from 'react-native';
import {StyleSheet, View, Image} from 'react-native';
import {LogoFruitHub} from '../../assets';

const SplashScreen = (props) => {
  useEffect(() => {
    setTimeout(() => props.navigation.reset({
      index: 0,
      routes: [{ name: 'Welcome' }],
    }), 1000);
  },
  [])

  return (
    <View style={styles.page}>
      <View>
        <Image source={LogoFruitHub} style={styles.image} />
      </View>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100%',
  },
  image: {
    width: 113,
    height: 164.94,
  },
});

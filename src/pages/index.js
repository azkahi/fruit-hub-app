import SplashScreen from './SplashScreen';
import WelcomeScreen from './WelcomeScreen';
import Authentication from './Authentication';
import OrderComplete from './OrderComplete';
import TrackOrder from './TrackOrder';
import Home from './Home';
import AddToBasket from './AddToBasket';
import MyBasket from './MyBasket';

export {SplashScreen, WelcomeScreen, Authentication, OrderComplete, TrackOrder, Home, MyBasket, AddToBasket};

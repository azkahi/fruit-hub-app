import React, {useState, useEffect} from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';
import {
  FruitBasketAuth,
  FruitDrops,
  EllipseWelcome,
  EllipseSmall,
} from '../../assets';

import fetchLogin from '../../redux/actions/authActions';
import { Button, InputHolder } from '../../components';

const Authentication = (props) => {
  const [username, setUsername] = useState('azka');

  const handleLogin = async () => {
    try {
      props.dispatchLogin(username);
    } catch (e) {
      alert (e)
      console.error(e);
    }
  };

  useEffect(() => {
    const { navigation } = props;

    if (props.authStore.payload.data?.token && props.authStore.payload.data?.token !== '') {
      AsyncStorage.setItem('@token', props.authStore.payload.data.token);
      navigation.reset({
        index: 0,
        routes: [{ name: 'Home' }],
      });
    }

  }, [props.authStore.payload.data]);

  return (
    <View style={styles.mainPage}>
      <View style={styles.topPage}>
        <Image source={FruitDrops} style={styles.fruitDrops} />
        <Image source={FruitBasketAuth} style={styles.fruitBasket} />
        <Image source={EllipseWelcome} />
      </View>
      <View style={styles.bottomPage}>
        <Text style={styles.mainText}>What is your firstname?</Text>
        <InputHolder placeholder="First Name" value={username} onChangeText={(val) => setUsername(val)} />
        <View style={{ height: 42 }} />
        <Button title={'Start Ordering'} onPress={() => handleLogin()} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  mainPage: {
    height: '100%',
  },
  topPage: {
    backgroundColor: '#FFA451',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    flex: 0.65,
  },
  fruitDrops: {
    width: 50,
    height: 37.52,
    marginLeft: 260,
    marginTop: 40,
  },
  fruitBasket: {
    height: 281.21,
    width: 301,
    marginBottom: 10,
  },
  bottomPage: {
    backgroundColor: '#E5E5E5',
    flex: 0.4,
    flexDirection: 'column',
  },
  mainText: {
    fontWeight: '500',
    fontStyle: 'normal',
    fontSize: 20,
    lineHeight: 28.6,
    marginTop: 30,
    marginLeft: 25,
    marginBottom: 16,
  },
});

function mapStateToProps(state) {
  return {
    authStore: state.authStore,
    isLoading: state.authStore.isLoading
  }
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchLogin: (username) => dispatch(fetchLogin(username)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Authentication);
